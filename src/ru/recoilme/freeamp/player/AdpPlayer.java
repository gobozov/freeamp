package ru.recoilme.freeamp.player;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import ru.recoilme.freeamp.ClsTrack;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: recoilme
 * Date: 28/11/13
 * Time: 17:25
 * To change this template use File | Settings | File Templates.
 */
public class AdpPlayer extends BaseAdapter {

    ArrayList<ClsTrack> data;
    Activity activity;
    float scale;

    public AdpPlayer(Activity activity, ArrayList<ClsTrack> data){
        this.data = data;
        this.activity = activity;
        scale = activity.getResources().getDisplayMetrics().density;
    }

    @Override
    public int getCount() {
        return data.size();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public long getItemId(int position) {
        return position;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LinearLayout linearLayout = new LinearLayout(activity);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        int dpAsPixels = (int) (10*scale + 0.5f);
        linearLayout.setPadding(dpAsPixels,dpAsPixels,dpAsPixels,dpAsPixels);

        ClsTrack o = data.get(position);

        final TextView artist = new TextView(activity);
        artist.setTextAppearance(activity,android.R.attr.textAppearanceMedium);
        artist.setText(o.getArtist());

        final TextView title = new TextView(activity);
        title.setTextAppearance(activity, android.R.attr.textAppearanceSmall);
        title.setText(o.getTitle());

        if (position==ViewPlayer.selected) {
            title.setTextColor(Color.parseColor("#FDC332"));
        }
        else {
            title.setTextColor(Color.LTGRAY);
        }
        linearLayout.addView(artist);
        linearLayout.addView(title);
        return linearLayout;
    }
}