/*
 * This is a simple foregroundservice example using Android 2.0 API. For more information, backward compatibility, etc. visit:
 * http://developer.android.com/resources/samples/ApiDemos/src/com/example/android/apis/app/ForegroundService.html
 */

package ru.recoilme.freeamp.player;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import com.androidquery.util.AQUtility;
import com.flurry.android.FlurryAgent;
import com.un4seen.bass.BASS;
import ru.recoilme.freeamp.ClsTrack;
import ru.recoilme.freeamp.NotificationUtil;
import ru.recoilme.freeamp.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Random;

public class ServicePlayer extends Service {


	// Notification
	private Notification notification;
	
	// Pending Intent to be called if a user click on the notification
	private PendingIntent pendIntent;

    private final static Random random = new Random();
    private boolean repeat;
    private int errorCount = 0;

    public boolean isShuffle() {
        return shuffle;
    }

    public void setShuffle(boolean shuffle){
        this.shuffle = shuffle;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    public void setActivityStarted(boolean activityStarted) {
        this.activityStarted = activityStarted;
    }

    // Bass Service Binder Class
	public class BassServiceBinder extends Binder {
		public ServicePlayer getService() {
            return ServicePlayer.this;
        }
    }
	
	// Channel Handle
	private int chan;
    //TrackList
    private ArrayList<ClsTrack> tracks = new ArrayList<ClsTrack>();
    //currentPosition
    private int position = 0;
    private int prevPosition = 0;
	
	// Activity with implemented BassInterface
	private InterfacePlayer activity;
	
	// Set Activity
	public void setActivity(InterfacePlayer activity) {
		this.activity = activity;
		if(activity != null) {
			activity.onPluginsLoaded(plugins);
			activity.onFileLoaded("", duration, "", "",0,0);
			activity.onProgressChanged(progress);
		}
	}

	// Properties: BassInterface
	private String plugins;
	private double duration = 0.0;
	private double progress = 0.0;
    private boolean shuffle;
    private boolean activityStarted;
	
	// Bass Service Binder
	private final IBinder mBinder = new BassServiceBinder();

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

    private TelephonyManager tm;
    enum RING_STATE {
        STATE_RINGING, STATE_OFFHOOK, STATE_NORMAL
    }

    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            if(BASS.BASS_ChannelIsActive(chan) == BASS.BASS_ACTIVE_PLAYING) {
                if(activity != null && activityStarted) {

                    progress = BASS.BASS_ChannelBytes2Seconds(chan, BASS.BASS_ChannelGetPosition(chan, BASS.BASS_POS_BYTE));
                    activity.onProgressChanged(progress);
                }
            }
            timerHandler.postDelayed(this, 200);//looks like laggy timer on more then 200 values
        }
    };

	@Override
	public void onCreate() {
		super.onCreate();

		// initialize default output device
		if (!BASS.BASS_Init(-1, 44100, 0)) {
			return;
		}

		// look for plugins
		plugins="";
        String path=getApplicationInfo().nativeLibraryDir;
		String[] list=new File(path).list();
		for (String s: list) {
			int plug=BASS.BASS_PluginLoad(path+"/"+s, 0);
			if (plug!=0) { // plugin loaded...
				plugins+=s+"\n"; // add it to the list
			}
		}
		if (plugins.isEmpty()) plugins="no plugins - visit the BASS webpage to get some\n";
		if(activity != null) {
			activity.onPluginsLoaded(plugins);
		}
		
		// Pending Intend
		Intent intent = new Intent(this, ViewPlayer.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		pendIntent = PendingIntent.getActivity(this, 0, intent, 0);
		
		// Prepare Notification
//		notification = new Notification(R.drawable.icon, "Player", System.currentTimeMillis());
//		notification.flags |= Notification.FLAG_NO_CLEAR;

        //tracklist
        updateTrackList();

        tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        tm.listen(telephone, PhoneStateListener.LISTEN_CALL_STATE);
	}

    public void updateTrackList(){
        String fileName = "tracks";
        try {
            FileInputStream fis = getApplicationContext().openFileInput(fileName);
            ObjectInputStream os = new ObjectInputStream(fis);
            tracks = (ArrayList<ClsTrack>) os.readObject();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
            //FlurryAgent.onError("6", "6", e);
        }
    }

    private PhoneStateListener telephone = new PhoneStateListener() {
        boolean onhook = false;
        RING_STATE callstaet;

        public void onCallStateChanged(int state, String number) {
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING: {
                    callstaet = RING_STATE.STATE_RINGING;
                    if (isPlaying()) {
                        Pause();
                        onhook = true;
                        //setResumeStop(CALL_RESUME);
                    }
                }
                break;
                case TelephonyManager.CALL_STATE_OFFHOOK: {
                    if (callstaet == RING_STATE.STATE_RINGING) {
                        callstaet = RING_STATE.STATE_OFFHOOK;
                    } else {
                        callstaet = RING_STATE.STATE_NORMAL;
                        if (isPlaying()) {
                            Pause();
                            onhook = true;
                            //setResumeStop(CALL_RESUME);
                        }
                    }
                }
                break;
                case TelephonyManager.CALL_STATE_IDLE: {
                    if (onhook) {
                        onhook = false;
                        if (isPaused())
                            playFromPause();
                        //setResumeStart(5, CALL_RESUME);
                    }
                    callstaet = RING_STATE.STATE_NORMAL;
                }
                break;
                default: {

                }
            }
        }
    };
	
	@Override
	public void onDestroy() {
        if (tm != null) {
            tm.listen(telephone, PhoneStateListener.LISTEN_NONE);
            tm = null;
        }
		// "free" the output device and all plugins
		BASS.BASS_Free();
		BASS.BASS_PluginFree(0);
		
		// Stop foreground
		stopForeground(true);
        stopUpdateProgress();

		super.onDestroy();
	}

	// translate a CTYPE value to text
	String GetCTypeString(int ctype, int plugin)
	{
		if (plugin!=0) { // using a plugin
			BASS.BASS_PLUGININFO pinfo=BASS.BASS_PluginGetInfo(plugin); // get plugin info
			int a;
			for (a=0;a<pinfo.formatc;a++) {
				if (pinfo.formats[a].ctype==ctype) // found a "ctype" match...
					return pinfo.formats[a].name; // return its name
			}
		}
		// check built-in stream formats...
		if (ctype==BASS.BASS_CTYPE_STREAM_OGG) return "Ogg Vorbis";
		if (ctype==BASS.BASS_CTYPE_STREAM_MP1) return "MPEG layer 1";
		if (ctype==BASS.BASS_CTYPE_STREAM_MP2) return "MPEG layer 2";
		if (ctype==BASS.BASS_CTYPE_STREAM_MP3) return "MPEG layer 3";
		if (ctype==BASS.BASS_CTYPE_STREAM_AIFF) return "Audio IFF";
		if (ctype==BASS.BASS_CTYPE_STREAM_WAV_PCM) return "PCM WAVE";
		if (ctype==BASS.BASS_CTYPE_STREAM_WAV_FLOAT) return "Floating-point WAVE";
		if ((ctype&BASS.BASS_CTYPE_STREAM_WAV)!=0) // other WAVE codec
			return "WAVE";
		return "?";
	}


    final BASS.SYNCPROC EndSync=new BASS.SYNCPROC() {
        public void SYNCPROC(int handle, int channel, int data, Object user) {
            playNext();
        }
    };

    // Play file
	public void Play(int pos) {
        startUpdateProgress();


        this.position = pos;


        AQUtility.debug("Play pos:",position);
		// Play File
        String path = "";
        if (tracks!=null && tracks.size()>position) {
            path = tracks.get(position).getPath();
        }
        if (path.equals("")) {
            onPlayError();
            return;
        }

        //BASS.BASS_MusicFree(chan);
        BASS.BASS_StreamFree(chan);
		if ((chan=BASS.BASS_StreamCreateFile(tracks.get(position).getPath(), 0, 0, BASS.BASS_SAMPLE_LOOP))==0) {

		    onPlayError();

			// Stop Foreground
			stopForeground(true);

			return;
		}

		// Play File
        int result = BASS.BASS_ChannelSetSync(chan, BASS.BASS_SYNC_END, 0, EndSync, 0);

        BASS.BASS_ChannelPlay(chan, false);

		// Update Properties
		this.duration = BASS.BASS_ChannelBytes2Seconds(chan, BASS.BASS_ChannelGetLength(chan, BASS.BASS_POS_BYTE));
		this.progress = 0.0;

		// Notify Activity
		if(activity != null) {
            AQUtility.debug("Playing title:",tracks.get(position).getTitle());
			activity.onFileLoaded(tracks.get(position).getPath(), this.duration,
                    tracks.get(position).getArtist(),
                    tracks.get(position).getTitle(),position,
                    tracks.get(position).getAlbumId());
			activity.onProgressChanged(progress);
		}
		
		// Start foreground
        fireNotification(pos);

	}

    private void fireNotification(int pos) {
        notification = NotificationUtil.getNotification(this, pendIntent, tracks.get(pos), isPlaying());
        startForeground(1, notification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();

        if ("play".equals(action)) {
            if (isPlaying())
                Pause() ;
            else
                playFromPause();
            //notification.contentView.setImageViewResource(R.id.action_play, isPlaying() ? R.drawable.base_pause : R.drawable.base_play);
            //startForeground(1, notification);
            fireNotification(position);
        } else if ("next".equals(action)){
            playNext();
        } else if ("prev".equals(action)){
            playPrev();
        }
        return START_NOT_STICKY;
    }

    public void playNext(){
        if (isShuffle()) {
            //TODO i may create stack with previos/next positions
            prevPosition = position;
            position = random.nextInt(tracks.size()-1);
            Play(position);
        }
        else {
            if (tracks.size()>(position+1)) {
                Play(position+1);
            }
            else if (tracks.size()>0){
                Play(0);
            }
        }
    }

    public void playPrev(){
        if (isShuffle()) {
            if (prevPosition>=0 && prevPosition<tracks.size()) {
                position = prevPosition;
            }
            else {
                return;
            }
        }
        else {
            if ((position-1)>=0) {
                position = position-1;
            }
            else {
                return;
            }
        }
        Play(position);
    }

    public void onPlayError() {
        // Update Properties
        this.duration = 0.0;
        this.progress = 0.0;

        // Notify activity
        if(activity != null) {
            activity.onFileLoaded(tracks.get(position).getPath(), this.duration, "", "", 0,0);
            activity.onProgressChanged(progress);
        }

        stopUpdateProgress();
        FlurryAgent.onError("onPlayError","","");
        //skip 1st 20 errors on play
        if (errorCount<20) {
            errorCount++;
            playNext();
        }
    }
	
	// Seek to position
	public void SeekTo(int progress) {
		BASS.BASS_ChannelSetPosition(chan, BASS.BASS_ChannelSeconds2Bytes(chan, progress), BASS.BASS_POS_BYTE);
	}

    public void Pause() {
        BASS.BASS_ChannelPause(chan);
        stopForeground(true);
        stopUpdateProgress();
    }

    public void Stop() {
        BASS.BASS_ChannelStop(chan);
        stopUpdateProgress();
    }

    public void stopUpdateProgress() {
        timerHandler.removeCallbacks(timerRunnable);
    }

    public void startUpdateProgress() {
        //start update progress
        timerHandler.postDelayed(timerRunnable, 0);
    }

    public boolean isPlaying() {
        if (!(BASS.BASS_ACTIVE_PLAYING == BASS.BASS_ChannelIsActive(chan))) {
            stopForeground(true);
            stopUpdateProgress();
        }
        if (BASS.BASS_ACTIVE_PLAYING == BASS.BASS_ChannelIsActive(chan)) {
            startUpdateProgress();
        }
        return BASS.BASS_ACTIVE_PLAYING ==BASS.BASS_ChannelIsActive(chan);
    }

    public boolean isPaused() {
        return BASS.BASS_ACTIVE_PAUSED ==BASS.BASS_ChannelIsActive(chan);
    }
    public void playFromPause() {
        AQUtility.debug("playFromPause");
        startUpdateProgress();
        BASS.BASS_ChannelPlay(chan, false);
    }
}
