package ru.recoilme.freeamp.playlist;

import android.content.Context;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.androidquery.util.AQUtility;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.flac.FlacFileReader;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.flac.FlacTag;
import org.jaudiotagger.tag.id3.AbstractID3v2Tag;
import org.jaudiotagger.tag.id3.ID3v24Frames;
import org.jaudiotagger.tag.vorbiscomment.VorbisCommentTag;
import ru.recoilme.freeamp.ClsTrack;

import java.io.File;

/**
 * Created by recoilme on 05/12/13.
 */
public class MakePlaylistFS extends MakePlaylistAbstract {

    public MakePlaylistFS(Context context) {
        super(context);
    }

    @Override
    public void getAllTracks(Context context) {

        t = System.currentTimeMillis();
        String scanDir = PreferenceManager.getDefaultSharedPreferences(context).getString("scanDir",Environment.getExternalStorageDirectory().getAbsolutePath());
        File currentDir = new File(scanDir);
        AQUtility.debug("currentDir",currentDir.getAbsolutePath());
        walk(currentDir);
        AQUtility.debug("time","(ms):"+(System.currentTimeMillis()-t)); //5000

    }

    public void add2list(String artist,String yearS,String trackS,String title,String album,String composer,
                         String path,String folder, long lastModified,String filename){
        int year = 0;
        int track = 0;

        try {
            if (!yearS.equals("")) {
                if (yearS.length()>3) {
                    yearS = yearS.substring(0,4);
                }
                year  = Integer.parseInt(yearS.replaceAll("[^\\d.]", ""));
            }

            if (!trackS.equals(""))
                track = Integer.parseInt(trackS.replaceAll("[^\\d.]", ""));
        } catch (Exception e) {AQUtility.debug(e.toString());}

        allTracks.add(new ClsTrack(
                artist.equals("") ?"<unknown>":artist,
                title.equals("") ?filename:title,
                album,
                composer,
                year,
                track,
                0,
                path,
                folder,
                lastModified,
                0));
    }

    public void walk(File root) {

        File[] list = root.listFiles();

        if (list==null) return;
        for (File f : list) {
            if (f.isDirectory()) {

                walk(f);
            }
            else {
                String path = f.getAbsolutePath().toLowerCase();
                if (path.endsWith(".mp3") || path.endsWith(".flac") || path.endsWith(".ogg")
                        || path.endsWith(".oga") || path.endsWith(".aac")
                        || path.endsWith(".m4a") || path.endsWith(".m4b") || path.endsWith(".m4p")
                        || path.endsWith(".wma") || path.endsWith(".wav") || path.endsWith(".wv")
                        || path.endsWith(".mpc") || path.endsWith(".ape")) {

                    String folder = "";
                    String[] pathArray = path.split(
                            TextUtils.equals(System.getProperty("file.separator"), "")?"/":System.getProperty("file.separator")
                    );
                    if (pathArray!=null && pathArray.length>1) {
                        folder = pathArray[pathArray.length-2];
                    }
                    long lastModified = f.lastModified();

                    String artist = "";
                    String title = "";
                    String album = "";
                    String composer = "";
                    String yearS = "";
                    String trackS = "";

                    //MP3
                    if (path.endsWith(".mp3")) {

                        try {
                            MP3File audioFile = (MP3File) AudioFileIO.read(f);
                            Tag tag = audioFile.getTag();

                            if (audioFile.hasID3v2Tag()) {
                                AbstractID3v2Tag v2Tag  = audioFile.getID3v2Tag();

                                artist = "" + v2Tag.getFirst(ID3v24Frames.FRAME_ID_ARTIST);
                                yearS = "" + v2Tag.getFirst(ID3v24Frames.FRAME_ID_YEAR);
                                trackS = "" + v2Tag.getFirst(ID3v24Frames.FRAME_ID_TRACK);
                                title = "" + v2Tag.getFirst(ID3v24Frames.FRAME_ID_TITLE);
                                album = "" + ""+v2Tag.getFirst(ID3v24Frames.FRAME_ID_ALBUM);
                                composer = "" + v2Tag.getFirst(ID3v24Frames.FRAME_ID_COMPOSER);
                            }
                            else {
                                artist = "" + tag.getFirst(FieldKey.ARTIST);
                                yearS = "" + tag.getFirst(FieldKey.YEAR);
                                trackS = "" + tag.getFirst(FieldKey.TRACK);
                                title = "" + tag.getFirst(FieldKey.TITLE);
                                album = "" + ""+tag.getFirst(FieldKey.ALBUM);
                                composer = "" + tag.getFirst(FieldKey.COMPOSER);
                            }
                            add2list(artist,yearS,trackS,title,album,composer,
                                    path,folder,lastModified,pathArray[pathArray.length-1]);
                        }
                        catch (Exception e) {
                            AQUtility.debug(e.toString());
                        }

                    }
                    else {
                        //all other format
                        try {
                            AudioFile audioFile =  AudioFileIO.read(f);
                            Tag tag = audioFile.getTag();

                            artist = "" + tag.getFirst(FieldKey.ARTIST);
                            yearS = "" + tag.getFirst(FieldKey.YEAR);
                            trackS = "" + tag.getFirst(FieldKey.TRACK);
                            title = "" + tag.getFirst(FieldKey.TITLE);
                            album = "" + ""+tag.getFirst(FieldKey.ALBUM);
                            composer = "" + tag.getFirst(FieldKey.COMPOSER);

                            add2list(artist,yearS,trackS,title,album,composer,
                                    path,folder,lastModified,pathArray[pathArray.length-1]);

                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        }
    }
}
